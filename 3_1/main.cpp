/*
 * Дано число N < 106 и последовательность пар целых чисел из [-231..231] длиной N.
 * Построить декартово дерево из N узлов, характеризующихся парами чисел {Xi, Yi}.
 * Каждая пара чисел {Xi, Yi} определяет ключ Xi и приоритет Yi в декартовом дереве.
 * Добавление узла в декартово дерево выполняйте второй версией алгоритма, рассказанного на лекции:
 * При добавлении узла (x, y) выполняйте спуск по ключу до узла P с меньшим приоритетом.
 * Затем разбейте найденное поддерево по ключу x так, чтобы в первом поддереве все ключи меньше x,
 * а во втором больше или равны x. Получившиеся два дерева сделайте дочерними для нового узла
 * (x, y). Новый узел вставьте на место узла P.
 *
 * Построить также наивное дерево поиска по ключам Xi методом из задачи 2.
 *
 * 3_1. Вычислить разницу глубин наивного дерева поиска и декартового дерева. Разница может быть отрицательна.
 *
 * Щербаков Сергей, номер в ведомости: 50
 */

#include <iostream>
#include <stack>
#include <queue>
#include <cmath>


// Height calculation class
template<class T>
struct TreeHeightCalculator {
    int getHeight (T* root) const;

    struct NodeAndHeight {
        T* node;
        int height;

        NodeAndHeight(T* node, int h): node(node), height(h) {}
    };
};

template<class T>
int TreeHeightCalculator<T>::getHeight (T* root) const {
    if (root == nullptr) {
        return 0;
    }

    int max = 0;

    std::queue<NodeAndHeight> queue;
    queue.push(NodeAndHeight(root, 1));

    while(!queue.empty()) {
        NodeAndHeight nodeAndHeight = queue.front();
        T* node = nodeAndHeight.node;
        queue.pop();
        if (nodeAndHeight.height > max) {
            max = nodeAndHeight.height;
        }
        if (node->left != nullptr) {
            queue.push(NodeAndHeight(node->left, nodeAndHeight.height + 1));
        }
        if (node->right != nullptr) {
            queue.push(NodeAndHeight(node->right, nodeAndHeight.height + 1));
        }
    }
    return max;
}

////////////////////////////////////////BINARY SEARCH TREE//////////////////////////////////////////////////////////////

template<class T>
struct DefaultComparator {
    bool isLess(const T &lhs, const T &rhs) {
        return lhs < rhs;
    }
};

template<class T, class C>
class BinaryTree {
public:
    explicit BinaryTree(const C &);

    ~BinaryTree();

    void Add(T key);

    int GetHeight() const;

    void InOrderPrint();

private:
    struct Node {
        Node *left;
        Node *right;
        T key;

        explicit Node(T key) : left(nullptr), right(nullptr), key(key) {}
    };

    TreeHeightCalculator<Node> calculator;

    Node *root;
    C comparator;
};

// Constructors && Destructors

template<class T, class C>
BinaryTree<T, C>::BinaryTree(const C &cmp):root(nullptr), comparator(cmp) {

}

template<class T, class C>
BinaryTree<T, C>::~BinaryTree() {
    if (root == nullptr) {
        return;
    }

    std::stack<Node *> stack;

    stack.push(root);

    Node *current = root->left;

    while (!stack.empty()) {

        if (current) {
            stack.push(current);
            current = current->left;
            continue;
        }

        Node *node = stack.top();

        if (node->right) {
            stack.push(node->right);
            current = node->right->left;
            continue;
        }

        stack.pop();
        if (!stack.empty()) {
            Node *parentNode = stack.top();
            if (parentNode->right == node) {
                parentNode->right = nullptr;
            } else {
                parentNode->left = nullptr;
            }
        }
        delete node;
        current = nullptr;
    }
}

// Public Methods
template<class T, class C>
int BinaryTree<T, C>::GetHeight() const {
    return calculator.getHeight(root);
}


template<class T, class C>
void BinaryTree<T, C>::Add(T key) {
    if (root == nullptr) {
        root = new Node(key);
        return;
    }

    Node *current = root;

    while (current) {
        if (!comparator.isLess(current->key, key)) {
            if (current->left == nullptr) {
                current->left = new Node(key);
                break;
            }
            current = current->left;
        } else {
            if (current->right == nullptr) {
                current->right = new Node(key);
                break;
            }
            current = current->right;
        }
    }
}

template<class T, class C>
void BinaryTree<T, C>::InOrderPrint() {
    if (root == nullptr) {
        return;
    }

    std::stack<Node *> stack;

    stack.push(root);

    Node *current = root->left;

    while (!stack.empty()) {

        if (current) {
            stack.push(current);
            current = current->left;
            continue;
        }

        Node *node = stack.top();
        stack.pop();
        std::cout << node->key << " ";

        if (node->right) {
            stack.push(node->right);
            current = node->right->left;
        }
    }
}

////////////////////////////////////////CARTESIAN TREE//////////////////////////////////////////////////////////////////

template<class T1, class C1, class T2, class C2>
class Treap {
public:
    Treap(const C1&, const C2&);
    ~Treap();

    void Add(T1 key, T2 priority);

    int GetHeight() const;

private:
    struct TreapNode {
        T1 key;
        T2 priority;
        TreapNode* left;
        TreapNode* right;

        TreapNode(T1 key, T2 priority): left(nullptr), right(nullptr), key(key), priority(priority) {}
    };

    TreapNode* root;
    C1 comparator1;
    C2 comparator2;

    TreeHeightCalculator<TreapNode> calculator;

    void split(TreapNode* curNode, T1 key, TreapNode*& left, TreapNode*& right);

    TreapNode* merge(TreapNode* left, TreapNode* right);

    void postOrderDelete(TreapNode* node);
};

// Constructors && Destructors

template<class T1, class C1, class T2, class C2>
Treap<T1, C1, T2, C2>::Treap(const C1& c1, const C2& c2):root(nullptr), comparator1(c1), comparator2(c2) {

}

template<class T1, class C1, class T2, class C2>
void Treap<T1, C1, T2, C2>::postOrderDelete(TreapNode* node) {
    if (node == nullptr) {
        return;
    }

    postOrderDelete(node->left);
    postOrderDelete(node->right);
    delete node;
}

template<class T1, class C1, class T2, class C2>
Treap<T1, C1, T2, C2>::~Treap() {
    postOrderDelete(root);
}

// Public Methods

template<class T1, class C1, class T2, class C2>
void Treap<T1, C1, T2, C2>::Add(T1 key, T2 priority) {
    if (root == nullptr) {
        root = new TreapNode(key, priority);
        return;
    }

    TreapNode* currentParent = nullptr;
    TreapNode* current = root;

    // find node with less priority
    while (current) {
        // found -> break
        if (comparator2.isLess(current->priority, priority)) {
            break;
        }

        currentParent = current;
        // else go next with key
        if (comparator1.isLess(key, current->key)) {

            if (current->left == nullptr) {
                break;
            }
            current = current->left;
        } else {
            if (current->right == nullptr) {
                break;
            }
            current = current->right;
        }
    }

    auto* newNode = new TreapNode(key, priority);

    // if current -> leaf with higher priority, just add element as leaf
    if (current->priority > priority) {

        if (current->key < key && current->right == nullptr) {
            current->right = newNode;
            return;
        } else if (key < current->key && current->left == nullptr){
            current->left = newNode;
            return;
        }
    }

    TreapNode* left;
    TreapNode* right;

    split(current, key, left, right);

    newNode->left = left;
    newNode->right = right;

    // if current - root with less priority
    if (currentParent == nullptr) {
        root = newNode;
        return;
    }

    // other cases
    if (comparator1.isLess(key, currentParent->key)) {
        currentParent->left = newNode;
    } else {
        currentParent->right = newNode;
    }

}

template<class T1, class C1, class T2, class C2>
int Treap<T1, C1, T2, C2>::GetHeight() const {
    return calculator.getHeight(root);
}

// Private Methods

template<class T1, class C1, class T2, class C2>
void Treap<T1, C1, T2, C2>::split(TreapNode* curNode, T1 key, TreapNode*& left, TreapNode*& right) {
    if(curNode == nullptr) {
        left = nullptr;
        right = nullptr;
    } else if(!comparator1.isLess(key, curNode->key) ) {
        split(curNode->right, key, curNode->right, right);
        left = curNode;
    } else {
        split(curNode->left, key, left, curNode->left);
        right = curNode;
    }
}

template<class T1, class C1, class T2, class C2>
typename Treap<T1, C1, T2, C2>::TreapNode* Treap<T1, C1, T2, C2>::merge(TreapNode* left, TreapNode* right) {
    if (left == nullptr || right == nullptr) {
        return left == nullptr ? right : left;
    }
    if (comparator2.isLess(right->priority, left->priority)) {
        left->right = merge(left->right, right);
        return left;
    } else {
        right->left = merge(left, right->left);
        return right;
    }
}

int main() {
    DefaultComparator<int> cmp1;
    DefaultComparator<int> cmp2;
    Treap<int, DefaultComparator<int>, int, DefaultComparator<int>> treap(cmp1, cmp2);
    BinaryTree<int, DefaultComparator<int>> tree(cmp1);

    int n;
    std::cin >> n;
    int key;
    int priority;
    for (int i = 0; i < n; ++i) {
        std::cin >> key >> priority;
        treap.Add(key, priority);
        tree.Add(key);
    }

    std::cout << abs(treap.GetHeight() - tree.GetHeight());
    return 0;
}
