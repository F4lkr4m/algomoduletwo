/*
 *
 * Требование для всех вариантов Задачи 4
 * Решение должно поддерживать передачу функции сравнения снаружи.
 *
 * 4_2. Порядковые статистики. Дано число N и N строк. Каждая строка содержит команду добавления или удаления
 * натуральных чисел, а также запрос на получение k-ой порядковой статистики.
 * Команда добавления числа A задается положительным числом A, команда удаления числа A задается отрицательным
 * числом “-A”. Запрос на получение k-ой порядковой статистики задается числом k.
 *
 * Требования: скорость выполнения запроса - O(log n).
 *
 * Щербаков Сергей, номер в ведомости: 50
 */

#include <iostream>
#include <stack>
#include <cassert>

template<class T>
struct DefaultComparator {
    bool isLess(const T& lhs, const T& rhs) {
        return lhs < rhs;
    }

    bool isEqual(const T& lhs, const T& rhs) {
        return lhs == rhs;
    }
};

template<class T, class C>
class AVLTree {
public:
    explicit AVLTree(const C&);
    ~AVLTree();

    void Insert(T key);

    bool Delete(T key);

    T GetKStat(int index);

private:
    struct AVLTreeNode // структура для представления узлов дерева
    {
        T key;
        unsigned char height;
        int nodesCount;
        AVLTreeNode* left;
        AVLTreeNode* right;
        explicit AVLTreeNode(T key): key(key), left(nullptr), right(nullptr), height(1), nodesCount(1) {}
    };

    AVLTreeNode* root;
    C comparator;


    bool deleteImpl(AVLTreeNode*& node, T key);

    void deleteNode(AVLTreeNode*& node);

    AVLTreeNode* findAndRemoveMin(AVLTreeNode* p, T* nodeKey);

    unsigned char height(AVLTreeNode* node);

    int count(AVLTreeNode* node);

    int balanceFactor(AVLTreeNode* node);

    void fixHeight(AVLTreeNode* node);

    void fixCount(AVLTreeNode* node);

    AVLTreeNode* rotateRight(AVLTreeNode* node);

    AVLTreeNode* rotateLeft(AVLTreeNode* node);

    AVLTreeNode* balance(AVLTreeNode* node);

    AVLTreeNode* insert(AVLTreeNode* p, T key);

    void postOrderDelete(AVLTreeNode* node);

    T getKStat(AVLTreeNode* node, int index);

};

// Constructors && Destructors

template<class T, class C>
AVLTree<T, C>::AVLTree(const C& cmp):comparator(cmp), root(nullptr) {}

template<class T, class C>
AVLTree<T, C>::~AVLTree() {
    if (root == nullptr) {
        return;
    }
    postOrderDelete(root);
}

// Public Methods

template<class T, class C>
bool AVLTree<T, C>::Delete(T key) {
    return deleteImpl(root, key);
}

template<class T, class C>
T AVLTree<T, C>::GetKStat(int index) {
    return getKStat(root, index);
}

template<class T, class C>
T AVLTree<T, C>::getKStat(AVLTreeNode* node, int index) {
    int currentNodeIndex = count(node->left);

    if (currentNodeIndex == index) {
        return node->key;
    }

    if (currentNodeIndex > index) {
        return getKStat(node->left, index);
    } else {
        return getKStat(node->right, index - count(node->left) - 1);
    }
}

template<class T, class C>
void AVLTree<T, C>::Insert(T key) {
    root = insert(root, key);
}

// Private Methods

template<class T, class C>
void AVLTree<T, C>::postOrderDelete(AVLTreeNode* node) {
    if (node == nullptr) {
        return;
    }
    postOrderDelete(node->left);
    postOrderDelete(node->right);
    delete node;
}

template<class T, class C>
bool AVLTree<T, C>::deleteImpl(AVLTreeNode*& node, T key) {
    if (node == nullptr) {
        return false;
    }

    if (comparator.isEqual(node->key,key)) {
        deleteNode(node);
        return true;
    }

    if (deleteImpl(comparator.isLess(key, node->key) ? node->left : node->right, key)) {
        node = balance(node);
        return true;
    }
    return false;
}

template<class T, class C>
void AVLTree<T, C>::deleteNode(AVLTreeNode*& node) {
    if (node->left == nullptr) {
        AVLTreeNode* right = node->right;
        delete node;
        node = right;
    } else if (node->right == nullptr) {
        AVLTreeNode* left = node->left;
        delete node;
        node = left;
    } else {
        node->right = findAndRemoveMin(node->right, &node->key);
    }

    if (node != nullptr) {
        node = balance(node);
    }
}

template<class T, class C>
typename AVLTree<T, C>::AVLTreeNode* AVLTree<T, C>::findAndRemoveMin(AVLTreeNode* p, T* nodeKey) {
    // go to the minimum
    if (p->left != nullptr) {
        p->left = findAndRemoveMin(p->left, nodeKey);
        return balance(p);
    }
    // write key of minimum
    *nodeKey = p->key;

    AVLTreeNode* temp = p->right;
    delete p;

    if (temp == nullptr) {
        // nothing to balance
        return nullptr;
    }

    return balance(temp);
}


template<class T, class C>
unsigned char AVLTree<T, C>::height(AVLTreeNode* p)
{
    return p ? p->height : 0;
}

template<class T, class C>
int AVLTree<T, C>::count(AVLTreeNode* p) {
    if (p == nullptr) {
        return 0;
    }
    return p->nodesCount;
}

template<class T, class C>
int AVLTree<T, C>::balanceFactor(AVLTreeNode* p)
{
    return height(p->right)-height(p->left);
}

template<class T, class C>
void AVLTree<T, C>::fixHeight(AVLTreeNode* p)
{
    unsigned char hl = height(p->left);
    unsigned char hr = height(p->right);
    p->height = (hl > hr ? hl : hr) + 1;
}

template<class T, class C>
void AVLTree<T, C>::fixCount(AVLTreeNode* node) {
    if (node == nullptr) {
        return;
    }
    int cl = count(node->left);
    int cr = count(node->right);
    node->nodesCount = cr + cl + 1;
}

template<class T, class C>
typename AVLTree<T, C>::AVLTreeNode* AVLTree<T, C>::rotateRight(AVLTreeNode* p) // правый поворот вокруг p
{
    AVLTreeNode* q = p->left;
    p->left = q->right;
    q->right = p;
    fixHeight(p);
    fixCount(p);
    fixHeight(q);
    fixCount(q);
    return q;
}

template<class T, class C>
typename AVLTree<T, C>::AVLTreeNode* AVLTree<T, C>::rotateLeft(AVLTreeNode* q) // левый поворот вокруг q
{
    AVLTreeNode* p = q->right;
    q->right = p->left;
    p->left = q;
    fixHeight(q);
    fixCount(q);
    fixHeight(p);
    fixCount(p);
    return p;
}

template<class T, class C>
typename AVLTree<T, C>::AVLTreeNode* AVLTree<T, C>::balance(AVLTreeNode* p) // балансировка узла p
{
    fixHeight(p);
    fixCount(p);
    if(balanceFactor(p) == 2) {
        if( balanceFactor(p->right) < 0 )
            p->right = rotateRight(p->right);
        return rotateLeft(p);
    }
    if( balanceFactor(p) == -2 ) {
        if( balanceFactor(p->left) > 0  )
            p->left = rotateLeft(p->left);
        return rotateRight(p);
    }
    return p; // балансировка не нужна
}

template<class T, class C>
typename AVLTree<T, C>::AVLTreeNode* AVLTree<T, C>::insert(AVLTreeNode* p, T k) // вставка ключа k в дерево с корнем p
{
    if( p == nullptr ) {
        return new AVLTreeNode(k);
    }
    if( comparator.isLess(k, p->key) )
        p->left = insert(p->left,k);
    else
        p->right = insert(p->right,k);
    return balance(p);
}

int main() {
    DefaultComparator<int> cmp1;
    AVLTree<int, DefaultComparator<int>> avlTree(cmp1);

    int n;
    std::cin >> n;
    int val;
    int indexForKStat;
    for (int i = 0; i < n; ++i) {
        std::cin >> val >> indexForKStat;
        if (val >= 0) {
            avlTree.Insert(val);
        } else {
            avlTree.Delete(abs(val));
        }
        std::cout << avlTree.GetKStat(indexForKStat) << " ";
    }

    return 0;
}
