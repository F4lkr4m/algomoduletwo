/*
 * Дано число N < 106 и последовательность целых чисел из [-231..231] длиной N.
 * Требуется построить бинарное дерево, заданное наивным порядком вставки.
 * Т.е., при добавлении очередного числа K в дерево с корнем root, если root→Key ≤ K,
 * то узел K добавляется в правое поддерево root; иначе в левое поддерево root.
 *
 * Требования: Рекурсия запрещена. Решение должно поддерживать передачу функции сравнения снаружи.
 *
 * 2_1. Выведите элементы в порядке in-order (слева направо).
 */

#include <iostream>
#include <stack>

template<class T>
struct DefaultComparator {
    bool isLess(const T &lhs, const T &rhs) {
        return lhs < rhs;
    }
};

template<class T, class C>
class BinaryTree {
public:
    BinaryTree(const C &);

    ~BinaryTree();

    void Add(T key);

    void InOrderPrint();

private:
    struct Node {
        Node *left;
        Node *right;
        T key;

        Node(T key) : left(nullptr), right(nullptr), key(key) {}
    };

    Node *root;
    C comparator;
};

// Constructors && Destructors

template<class T, class C>
BinaryTree<T, C>::BinaryTree(const C &cmp):root(nullptr), comparator(cmp) {

}

template<class T, class C>
BinaryTree<T, C>::~BinaryTree() {

}

// Public Methods

template<class T, class C>
void BinaryTree<T, C>::Add(T key) {
    if (root == nullptr) {
        root = new Node(key);
        return;
    }

    Node *current = root;

    while (current) {
        if (!comparator.isLess(current->key, key)) {
            if (current->left == nullptr) {
                current->left = new Node(key);
                break;
            }
            current = current->left;
        } else {
            if (current->right == nullptr) {
                current->right = new Node(key);
                break;
            }
            current = current->right;
        }
    }
}

template<class T, class C>
void BinaryTree<T, C>::InOrderPrint() {
    if (root == nullptr) {
        return;
    }

    std::stack<Node*> stack;

    stack.push(root);

    Node* current = root->left;

//    while (!stack.empty()) {
//
//        if (current) {
//            stack.push(current);
//            current = current->left;
//            continue;
//        }
//
//        Node* node = stack.top();
//        stack.pop();
//        std::cout << node->key << " ";
//
//        if (node->right) {
//            stack.push(node->right);
//            current = node->right->left;
//        }
//    }

    Node* prev;
    while (!stack.empty()) {

        if (current) {
            stack.push(current);
            current = current->left;
            continue;
        }

        Node* node = stack.top();

        if (node->right && node->right !=  prev) {
            stack.push(node->right);
            current = node->right->left;
            continue;
        }

        prev = node;
        stack.pop();
        std::cout << node->key << " ";
        current = nullptr;
    }

}


int main() {
    DefaultComparator<int> cmp;
    BinaryTree<int, DefaultComparator<int>> tree(cmp);

    int n;
    std::cin >> n;

    int val;
    for (int i = 0; i < n; ++i) {
        std::cin >> val;
        tree.Add(val);
    }
    tree.InOrderPrint();

    return 0;
}
