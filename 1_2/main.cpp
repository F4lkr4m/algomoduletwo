/*
 * Реализуйте структуру данных типа “множество строк” на основе динамической хеш-таблицы с открытой адресацией.
 * Хранимые строки непустые и состоят из строчных латинских букв.
 * Хеш-функция строки должна быть реализована с помощью вычисления значения многочлена методом Горнера.
 * Начальный размер таблицы должен быть равным 8-ми. Перехеширование выполняйте при добавлении элементов в случае,
 * когда коэффициент заполнения таблицы достигает 3/4.
 *
 * Для разрешения коллизий используйте двойное хеширование.
 *
 * Щербаков Сергей, номер в ведомости: 50
 */

#include <iostream>
#include <cassert>
#include <string>
#include <vector>

struct StringHasher {
    unsigned int operator()(const std::string &str) const {
        int hash = 0;
        for (int i = 0; i < str.size(); ++i) {
            hash = hash * 7 + str[i];
        }
        return hash;
    }
};

struct StringHasher2 {
    unsigned int operator()(const std::string &str) const {
        return str.size();
    }
};

template<class T, class H1, class H2>
class HashTable {
public:
    explicit HashTable(const H1 &, const H2 &);

    HashTable(const HashTable &) = delete;

    HashTable &operator=(const HashTable &) = delete;

    ~HashTable();

    bool Add(const T &data);

    bool Has(const T &data) const;

    bool Delete(const T &data);

    void PrintTable() const;

private:
    enum Status {
        EMPTY,
        DELETED,
        DATA
    };

    struct HashTableNode {
        T data;
        Status status;
        unsigned int hash;

        HashTableNode() : hash(0), status(EMPTY) {}

        HashTableNode(const T &data, unsigned int hash) : data(data), hash(hash), status(DATA) {}

        // function that change data in struct
        void fillWithData(T newData, Status newStatus, unsigned int newHash) {
            data = newData;
            status = newStatus;
            hash = newHash;
        }
    };

    H1 hasher1;
    H2 hasher2;
    std::vector<HashTableNode> table;
    int keysCount;

    void growTable();
};

// Constructors && Destructors
template<class T, class H1, class H2>
HashTable<T, H1, H2>::HashTable(const H1 &hasher1, const H2 &hasher2):
        hasher1(hasher1),
        hasher2(hasher2),
        table(8),
        keysCount(0) {}

template<class T, class H1, class H2>
HashTable<T, H1, H2>::~HashTable() {

}

// Public Methods

// Если Del - запоминаем позицию первой такой ячейки firstDelPos идем дальше
// Если Data - Data == data ? return false : идем дальше
// Если Empty - вставляем в firstDelPos, если возможно, или в текущую если firstDelPos == -1
// Сделали table.size() проб - assert( firstDelPos != -1 ) - вставляем

template<class T, class H1, class H2>
bool HashTable<T, H1, H2>::Add(const T &data) {
    if (keysCount > table.size() * 3 / 4) {
        growTable();
    }

    unsigned int absoluteHash = hasher1(data);
    // step with second hash
    unsigned int step = (hasher2(data) * 2 + 1) % table.size();

    long firstDelPos = -1;

    unsigned int index;

    HashTableNode *node;

    for (int i = 0; i < table.size(); ++i) {
        index = (absoluteHash + i * step) % table.size();
        node = &table[index];

        if (node->status == DELETED && firstDelPos == -1) {
            firstDelPos = index;
        }

        if (node->status == EMPTY) {
            unsigned int placeForNode;
            if (firstDelPos != -1) {
                placeForNode = firstDelPos;
            } else {
                placeForNode = index;
            }
            table[placeForNode].fillWithData(data, DATA, absoluteHash);

            ++keysCount;
            return true;
        }

        if (node->data == data) {
            return false;
        }
    }

    //assert(firstDelPos != -1);

    table[firstDelPos].fillWithData(data, DATA, absoluteHash);

    ++keysCount;
    return true;
}

template<class T, class H1, class H2>
void HashTable<T, H1, H2>::growTable() {
    std::vector<HashTableNode> newTable(table.size() * 2, HashTableNode());

    // index of node in new table
    unsigned int index;
    for (int i = 0; i < table.size(); ++i) {
        if (table[i].status != DATA) {
            continue;
        }

        unsigned int step = (hasher2(table[i].data) * 2 + 1) % newTable.size();

        int j = 0;
        index = (table[i].hash + j * step) % newTable.size();
        while (newTable[index].status != EMPTY) {
            index = (table[i].hash + j * step) % newTable.size();
            ++j;
        }

        newTable[index].fillWithData(table[i].data, DATA, table[i].hash);
    }
    table = std::move(newTable);
}

// Пробируемся не более table.size() раз, если больше - return false
// Если Del - идем дальше
// Если Data - Data == data ? return true : идем дальше
// Если Empty - return false

template<class T, class H1, class H2>
bool HashTable<T, H1, H2>::Has(const T &data) const {
    unsigned int absoluteHash = hasher1(data);
    unsigned int step = (hasher2(data) * 2 + 1) % table.size();

    // index of node
    unsigned int index;

    for (int i = 0; i < table.size(); ++i) {
        index = (absoluteHash + i * step) % table.size();

        if (table[index].status == DELETED) {
            continue;
        }

        if (table[index].status == DATA && table[index].data == data) {
            return true;
        }

        if (table[index].status == EMPTY) {
            return false;
        }
    }

    return false;
}

// Пробируемся не более table.size() раз, если больше - return false
// Если Del - идем дальше
// Если Data - Data == data ? помечаем Del и return true : идем дальше
// Если Empty - return false

template<class T, class H1, class H2>
bool HashTable<T, H1, H2>::Delete(const T &data) {
    unsigned int absoluteHash = hasher1(data);
    unsigned int step = (hasher2(data) * 2 + 1) % table.size();

    unsigned int index;

    for (int i = 0; i < table.size(); ++i) {
        index = (absoluteHash + i * step) % table.size();

        if (table[index].status == DELETED) {
            continue;
        }

        if (table[index].status == DATA && table[index].data == data) {
            table[index].status = DELETED;
            --keysCount;
            return true;
        }

        if (table[index].status == EMPTY) {
            return false;
        }
    }

    return false;
}

template<class T, class H1, class H2>
void HashTable<T, H1, H2>::PrintTable() const {
    for (int i = 0; i < table.size(); ++i) {
        if (table[i].status == DATA) {
            std::cout << table[i].data << " ";
        }
    }
    std::cout << "\n";
}

int main() {
    StringHasher hasher;
    StringHasher2 hasher2;
    HashTable<std::string, StringHasher, StringHasher2> table(hasher, hasher2);

    char operation = 0;
    std::string line;

    while (std::cin >> operation >> line) {
        switch (operation) {
            case '+':
                std::cout << (table.Add(line) ? "OK" : "FAIL") << "\n";
                break;
            case '-':
                std::cout << (table.Delete(line) ? "OK" : "FAIL") << "\n";
                break;
            case '?':
                std::cout << (table.Has(line) ? "OK" : "FAIL") << "\n";
                break;
            default:
                assert(false);
        }
    }

    return 0;
}
